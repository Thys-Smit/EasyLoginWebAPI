/*
 * @Author: Thys Smit
 * @Date: 2017-11-23 11:48:04
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2018-03-07 15:26:31
 */
global.__basedir = __dirname

/* module requires */
const express = require('express')
const bodyParser = require('body-parser')
const sql = require('mssql')
const fs = require('fs')
const logging = require('./helpers/logging.js')

/* require config */
var config = require('./config/config.js')

/* Require all routes */
var employeeRouter = require('./routes/employeeRouter.js')
var reportsRouter = require('./routes/reportsRouter.js')

var app = express()

/* define middleware */
// cors - chrome blocked 2nd server connection
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

/* basic logging to fs */
app.use((req, res, next) => {
  logging.log(`${req.method} ${req.url}`)
  next()
})

app.use(bodyParser.json()) // Allow JSON to be parsed within a payload.
app.use(bodyParser.urlencoded({ extended: false })) // Allow URL encoded messages to be parsed within a payload.
app.use(bodyParser.text()) // Allow plain text to be parsed within a payload.

var msSQLInstance = sql.connect(config.dbConfig, function (err, result) {
  if (err) logging.log('Connection could not be made.' + err)
  else logging.log('Connection has been established to ' + config.dbConfig.database)
})

// app.use(express.static(__dirname + '/views')) // Serve the views folder.
// app.use(express.static(__dirname + '/scripts')) // Serve the client side javascript.

app.use(employeeRouter) // Register the sql router to express.
app.use(reportsRouter) // Register the sql router to express.

// Create the server.
var server = app.listen(2022, () => {
  logging.log('Server listening on port ' + server.address().port)
})

// process.on('exit', function() {
//     // Add shutdown logic here.
//     logging.log('Server shutting down.')
//   });

module.exports = app
