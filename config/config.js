var dbConfig = {
  user: '',
  password: '',
  server: '',
  port: '',
  database: 'EasyLoginWeb'
}

// closure perhaps needed to fix scope issue here
var logConfig = {
  filename: 'server.log',
  folder: '\\logs\\',
  logfile: () => {
    return global.__basedir + '\\logs\\server.log'
  },
  logType: 'console'
}

module.exports.dbConfig = dbConfig
module.exports.logConfig = logConfig
