var express = require('express')
var router = express.Router()
const sql = require('mssql')
var moment = require('moment')
const logging = require('../helpers/logging.js')

var baseRoute = '/api/employee'

/*
 * lists all active employees on system
 */
router.get(`${baseRoute}/list/:date`, function (req, res) {

  var SQLRequest = new sql.Request()
  SQLRequest.input('Date', req.params.date)
  SQLRequest.execute('sp_EmployeeList', (err, result) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(200).send(result)
    }
  })

})

router.post(`${baseRoute}/add/:name&:surname`, function (req, res) {
  var SQLRequest = new sql.Request()
  SQLRequest.input('NAME', req.params.name)
  SQLRequest.input('Surname', req.params.surname)
  SQLRequest.execute('sp_ADD_Employees', (err, result) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(200).send(result)
    }
  })
})

/* Sign in required */
/* Example of call: localhost:2022/api/employee/loginToggle/6&2018-03-06T17:35:00.000Z */
router.post(`${baseRoute}/loginToggle/:id&:date`, (req, res) => {
  // remember to validate input params
  var SQLRequest = new sql.Request()
  SQLRequest.input('ID', req.params.id)
  SQLRequest.input('Date', req.params.date)
  logging.log(`date recived for toglle: ${req.params.date}`)
  SQLRequest.execute('sp_LoginOut', (err, result) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(200).send({
        result
      })
    }
  })
})

/* delete employee needed */

/* disable employee needed */

module.exports = router
