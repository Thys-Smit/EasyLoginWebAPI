var express = require('express')
var router = express.Router()
const sql = require('mssql')
const moment = require('moment')
const logging = require('../helpers/logging.js')

/*
 * Sp for displaying the grid for a specific employee for a specific week
 * Main grid
 */
router.get('/api/reports/hoursSpecWeek_grid/:id&:date', function (req, res) {
  /* validate input params before calling to sql + return error if needed */
  var SQLRequest = new sql.Request()
  SQLRequest.input('WeekStartDate', req.params.date)
  SQLRequest.input('IDEmployee', req.params.id)
  SQLRequest.execute('sp_LogInAndOut_PerWeek_Grid', (err, result) => {
    if (err) {
      res.status(400).send(err)
    } else {
      // logging.log('result retrieved')
      // logging.log(JSON.stringify(result, null, 2))
      res.status(200).send(result)
    }
  })
})

/*
* returns the details (un pivoted) per user for the date specified for end date
*/
router.get('/api/reports/hoursSpecWeek/:id&:date', function (req, res) {
  var date = moment(req.params.date)
  var endDate = date.format('YYYY-MM-DD')
  date = date.subtract(7, 'days')
  var startDate = date.format('YYYY-MM-DD')

  console.log(`Before Moment ${req.params.date} -- Original date passed in with moment ${date} Start date ${startDate} and end date ${endDate} and seven days ago ${date}`)

  var SQLRequest = new sql.Request()
  SQLRequest.input('ID', req.params.id)
  SQLRequest.input('firstDate', endDate.toString())
  SQLRequest.input('secondDate', startDate.toString())
  SQLRequest.execute('getUserHours', (err, result) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(200).send(result)
    }
  })
})

router.get('/api/reports/Summary_specUser3Week/:id&:date', function (req, res) {
  var SQLRequest = new sql.Request()
  SQLRequest.input('Today', req.params.date)
  SQLRequest.input('IDEmployee', req.params.id)
  SQLRequest.input('LunchTime', 0.75)
  SQLRequest.execute('sp_TQMconverted_hours_for3weeks', (err, result) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(200).send(result)
    }
  })
})

/*
* Return a summary of all the users
FKEmployee, FullName, HoursWorked
*/
router.get('/api/reports/Summary_allUsersTotalHrs/:date', function (req, res) {
  var SQLRequest = new sql.Request()
  SQLRequest.input('Today', req.params.date)
  SQLRequest.execute('sp_lastWeek_HoursWorked', (err, result) => {
    if (err) {
      res.status(400).send(err)
    } else {
      res.status(200).send(result)
    }
  })
})
module.exports = router
