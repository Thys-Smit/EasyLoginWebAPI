const fs = require('fs')
const config = require('../config/config.js')

function log_toLogFile (logMessage) {
  var now = new Date().toString()
  var log = `${now}: ${logMessage}`
  // console.log(config.logConfig.logfile())
  // creatae 'logs' folder
  // console.log(global.__basedir)
  // console.log(config.logConfig.folder)
  if (!fs.existsSync(global.__basedir + config.logConfig.folder)) {
    fs.mkdirSync(global.__basedir + config.logConfig.folder)
  }

  fs.appendFile(config.logConfig.logfile(), log + '\n', (err) => {
    if (err) {
      console.log('Unable to append server file log')
    }
  })
}

function log_toConsole (logMessage) {
  var now = new Date().toString()
  var log = `${now}: ${logMessage}`
  console.log(log)
}

function log_toAny (logMessage) {
  switch (config.logConfig.logType) {
    case 'file':
      log_toLogFile(logMessage)
      break
    case 'console':
      log_toConsole(logMessage)
      break
    case 'none':
      // I am not logging - made you look!
      break
    default:
      log_toConsole(`Your logging type does not exist: type provided '${config.logConfig.logType}'`)
      break
  }
}

module.exports = {
  log: log_toAny
}
